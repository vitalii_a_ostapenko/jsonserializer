package com.goitlearning.json.writer;

import java.io.IOException;
import java.io.Writer;

/**
 * This class overrides the methods of superclass and adds the symbols of white space, new line to
 * the methods of superclass {@link JsonWriter}
 * {@link #currentLevel} - the depth of indents, is used for determine indent's count
 * {@link #indicatorOfPropertySeparator} - the sign colon changes this value to true, is used for
 * writing values without indent after colon
 */
public class IndentedJsonWriter extends JsonWriter {

    private int currentLevel = 0;
    private boolean indicatorOfPropertySeparator = false;

    /**
     * Constructor calls constructor of the super class.
     *
     * @param writer - the stream {@link Writer} to write JSON.
     */
    public IndentedJsonWriter(Writer writer) {
        super(writer);
    }

    //Method writes two white spaces multiplied by currentLevel
    private void writeIndent() {
        for (int i = 0; i < currentLevel; i++) {
            try {
                writer.write("  ");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Method writes new line
    private void writeNewLine() {
        try {
            writer.write("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes a sign "{" and if there is not previous colon, writes new line after "{" ;
     * also this method increments {@link #currentLevel}
     */
    @Override
    public void writeObjectBegin() {
        templateWrite(() -> {
            super.writeObjectBegin();
            writeNewLine();
        });
        currentLevel++;
    }

    /**
     * Method writes a sign "}" from the new line and decrements {@link #currentLevel}
     */
    @Override
    public void writeObjectEnd() {
        currentLevel--;
        writeNewLine();
        writeIndent();
        super.writeObjectEnd();
        if (currentLevel != 0) writeNewLine();
    }

    /**
     * Method writes a sign "[" and if there is not previous colon, writes new line after "[" ;
     * also this method increments {@link #currentLevel}
     */
    @Override
    public void writeArrayBegin() {
        templateWrite(() -> {
            super.writeArrayBegin();
            writeNewLine();
        });
        currentLevel++;
    }

    /**
     * Method writes a sign "]" from the new line and decrements {@link #currentLevel}
     */
    @Override
    public void writeArrayEnd() {
        currentLevel--;
        writeNewLine();
        writeIndent();
        super.writeArrayEnd();
    }

    /**
     * Method writes in double quotes taken value
     *
     * @param value - String value and if there is not previous colon, writes indent before value
     */
    @Override
    public void writeString(String value) {
        templateWrite(() -> super.writeString(value));
    }

    /**
     * Method writes in double quotes taken in value
     *
     * @param value - character value and if there is not previous colon, writes indent before value
     */
    @Override
    public void writeChar(char value) {
        templateWrite(() -> super.writeChar(value));
    }

    /**
     * Method writes number
     *
     * @param value - number and if there is not previous colon, writes indent before value
     */
    @Override
    public void writeNumber(Number value) {
        templateWrite(() -> super.writeNumber(value));
    }

    /**
     * Method writes comma and write new line after
     */
    @Override
    public void writeSeparator() {
        super.writeSeparator();
        writeNewLine();
    }

    /**
     * Method writes colon with white space after, changes the boolean
     * {@link #indicatorOfPropertySeparator} to true
     */
    @Override
    public void writePropertySeparator() {
        super.writePropertySeparator();
        try {
            writer.write(" ");
        } catch (IOException e) {
            e.printStackTrace();
        }
        indicatorOfPropertySeparator = true;
    }

    /**
     * Method writes boolean value as text
     *
     * @param value - boolean value and if there is not previous colon, writes indent
     *              before value
     */
    @Override
    public void writeBoolean(boolean value) {
        templateWrite(() -> super.writeBoolean(value));
    }

    /**
     * Method writes null as text and if there is not previous colon, writes indent
     * before value
     */
    @Override
    public void writeNull() {
        templateWrite(super::writeNull);
    }

    /*
     * Method includes the same part of code of previous methods and takes in the empty method of
     * DifferenceWriter interface
     * This method changes indicatorOfPropertySeparator from true to false, or write indent
     * before calling the method of interface
     */
    private void templateWrite(DifferenceWriter differenceWriter) {
        if (indicatorOfPropertySeparator) {
            differenceWriter.writeDifference();
            indicatorOfPropertySeparator = false;
        } else {
            writeIndent();
            differenceWriter.writeDifference();
        }
    }

    /*
     * Interface includes only empty method that give ability to use methods with values
     * and without its in lambda-sentences in previous methods for make code easier
     */
    private interface DifferenceWriter {
        void writeDifference();
    }
}
