package com.goitlearning.json.writer;

public interface IJsonWriter {
    void writeObjectBegin();
    void writeObjectEnd();
    void writeArrayBegin();
    void writeArrayEnd();
    void writeString(String value);
    void writeChar(char value);
    void writeNumber(Number value);
    void writeSeparator();
    void writePropertySeparator();
    void writeBoolean(boolean value);
    void writeNull();
}
