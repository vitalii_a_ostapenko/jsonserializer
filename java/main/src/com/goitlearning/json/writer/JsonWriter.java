package com.goitlearning.json.writer;

import java.io.IOException;
import java.io.Writer;

/**
 * Class JsonWriter determines the rules for writing objects, implements the interface
 *
 * @see IJsonWriter
 */

public class JsonWriter implements IJsonWriter {

    protected Writer writer;

    /**
     * Constructor take in the next parameter:
     *
     * @param writer - the stream {@link Writer} to write JSON
     */
    public JsonWriter(Writer writer) {
        this.writer = writer;
    }

    /**
     * Method writes char value in double quotes, example: "a"
     *
     * @param value - taken in a char value
     */
    @Override
    public void writeChar(char value) {
        try {
            writer.write("\"" + value + "\"");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes a sign "{" in the begin of object, example: {(object)
     */
    @Override
    public void writeObjectBegin() {
        try {
            writer.write("{");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes a sign "}" in the end of object, example: (object)}
     */
    @Override
    public void writeObjectEnd() {
        try {
            writer.write("}");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes a sign "[" in the begin of array, example: [(array)
     */
    @Override
    public void writeArrayBegin() {
        try {
            writer.write("[");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes a sign "]" in the end of array, example: (array)]
     */
    @Override
    public void writeArrayEnd() {
        try {
            writer.write("]");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes String value in double quotes, example: "any text"
     *
     * @param value - taken in String value
     */
    @Override
    public void writeString(String value) {
        try {
            writer.write("\"" + value + "\"");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method write number as a text
     *
     * @param value - a number
     */
    @Override
    public void writeNumber(Number value) {
        try {
            writer.write(String.valueOf(value));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes comma
     */
    @Override
    public void writeSeparator() {
        try {
            writer.write(",");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes colon
     */
    @Override
    public void writePropertySeparator() {
        try {
            writer.write(":");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes boolean value as a text
     */
    @Override
    public void writeBoolean(boolean value) {
        try {
            writer.write(String.valueOf(value));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes null as a text
     */
    @Override
    public void writeNull() {
        try {
            writer.write("null");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
