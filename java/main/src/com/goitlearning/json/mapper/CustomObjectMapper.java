package com.goitlearning.json.mapper;

import com.goitlearning.json.mapper.annotations.JsonIgnore;
import com.goitlearning.json.mapper.annotations.JsonProperty;
import com.goitlearning.json.mapper.annotations.JsonSerializedName;
import com.goitlearning.json.serializer.ISubElementSerializer;
import com.goitlearning.json.writer.IJsonWriter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import static java.lang.reflect.Modifier.*;

/**
 * Used for writing objects of different type using {@link java.lang.reflect}
 */
public class CustomObjectMapper implements IJsonMapper {
    private ISubElementSerializer serializer;

    /**
     * Constructor initialize parameter {@link #serializer}
     *
     * @param serializer - defines way to serialize child objects.
     */
    public CustomObjectMapper(ISubElementSerializer serializer) {
        this.serializer = serializer;
    }

    /**
     * By default writes public, protected, package local and fields marked with {@link JsonProperty},
     * {@link JsonSerializedName}.
     * Ignores private, static, transient and fields with any modifier, annotations marked with {@link JsonIgnore}.
     * Also writes corresponding fields from super classes of given object.
     *
     * @param object - object for writing to writer
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(Object object, IJsonWriter writer) {
        Set<Field> fields = getFieldsIncludingSuper(object);
        Iterator<Field> iterator = fields.stream()
                .filter(field -> field.getDeclaredAnnotation(JsonIgnore.class) == null)
                .filter(field -> ((!isPrivate(field.getModifiers())
                        && !isTransient(field.getModifiers())
                        && !isStatic(field.getModifiers()))

                        || field.getDeclaredAnnotation(JsonProperty.class) != null
                        || field.getDeclaredAnnotation(JsonSerializedName.class) != null)).iterator();

        writer.writeObjectBegin();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            if (!field.isAccessible()) {
                field.setAccessible(true);
                writeField(field, object, writer);
                field.setAccessible(false);
                if (iterator.hasNext()) writer.writeSeparator();
            } else writeField(field, object, writer);
        }
        writer.writeObjectEnd();
    }

    //Collects all fields from current class and it super classes to Set
    private Set<Field> getFieldsIncludingSuper(Object object) {
        LinkedHashSet<Field> fields = new LinkedHashSet<>();
        Class<?> currentClass = object.getClass();
        while (currentClass != Object.class) {
            fields.addAll(Arrays.asList(currentClass.getDeclaredFields()));
            currentClass = currentClass.getSuperclass();
        }
        return fields;
    }

    //Writes field and writes it name accordingly to marked JsonSerializedName
    private void writeField(Field field, Object object, IJsonWriter writer) {
        if (field.getDeclaredAnnotation(JsonSerializedName.class) == null) writer.writeString(field.getName());
        else writer.writeString(field.getDeclaredAnnotation(JsonSerializedName.class).value());
        writer.writePropertySeparator();
        try {
            serializer.serialize(field.get(object), writer);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
