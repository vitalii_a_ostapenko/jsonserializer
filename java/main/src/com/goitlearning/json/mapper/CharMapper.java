package com.goitlearning.json.mapper;

import com.goitlearning.json.writer.IJsonWriter;

/**
 * The class writes {@link Character} to JSON.
 */
public class CharMapper implements IJsonMapper<Character> {

    /**
     * Method writes {@link Character}, for write uses classes that implement {@link IJsonWriter}
     *
     * @param object - {@link Character} for writing to JSON.
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(Character object, IJsonWriter writer) {
        writer.writeChar(object);
    }
}
