package com.goitlearning.json.mapper;

import com.goitlearning.json.writer.IJsonWriter;

/**
 * The class writes {@link Number} to JSON.
 */
public class NumberMapper implements IJsonMapper<Number> {
    /**
     * Method writes {@link Number}, for write uses classes that implement {@link IJsonWriter}
     *
     * @param object - {@link Number} for writing to JSON.
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(Number object, IJsonWriter writer) {
        writer.writeNumber(object);
    }
}
