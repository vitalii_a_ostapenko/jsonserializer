package com.goitlearning.json.mapper;

import com.goitlearning.json.writer.IJsonWriter;

/**
 * Defines mappers for serialization different objects.
 */
public interface IJsonMapper<T> {
    /**
     * @param object - object for writing to JSON.
     * @param writer - defines rules for writing to JSON.
     */
    void write(T object, IJsonWriter writer);
}
