package com.goitlearning.json.mapper;

import com.goitlearning.json.writer.IJsonWriter;

/**
 * The class writes {@link String} to JSON.
 */
public class StringMapper implements IJsonMapper<String> {
    /**
     * Method writes {@link String}, for write uses classes that implement {@link IJsonWriter}
     *
     * @param object - {@link String} for writing to JSON.
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(String object, IJsonWriter writer) {
        writer.writeString(object);
    }
}
