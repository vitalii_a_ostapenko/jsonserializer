package com.goitlearning.json.mapper;

import com.goitlearning.json.writer.IJsonWriter;

/**
 * The class writes null to JSON.
 */
public class NullMapper implements IJsonMapper {
    /**
     * Method writes null, for write uses classes that implement {@link IJsonWriter}
     *
     * @param object - null for writing to JSON.
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(Object object, IJsonWriter writer) {
        writer.writeNull();
    }
}
