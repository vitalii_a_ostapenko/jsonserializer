package com.goitlearning.json.mapper;

import com.goitlearning.json.writer.IJsonWriter;

/**
 * The class writes {@link Boolean} object
 */
public class BooleanMapper implements IJsonMapper<Boolean> {
    /**
     * Method writes {@link Boolean} object, for write uses classes that implement {@link IJsonWriter}
     *
     * @param object - boolean for writing to JSON.
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(Boolean object, IJsonWriter writer) {
        writer.writeBoolean(object);
    }
}
