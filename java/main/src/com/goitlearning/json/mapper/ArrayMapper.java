package com.goitlearning.json.mapper;

import com.goitlearning.json.serializer.ISubElementSerializer;
import com.goitlearning.json.writer.IJsonWriter;

import static java.lang.reflect.Array.get;
import static java.lang.reflect.Array.getLength;

/**
 * The class writes instance of primitive or object array to JSON.
 */
public class ArrayMapper implements IJsonMapper {
    private ISubElementSerializer serializer;

    /**
     * Constructor initialize parameter {@link #serializer}
     *
     * @param serializer - defines way to serialize child objects.
     */
    public ArrayMapper(ISubElementSerializer serializer) {
        this.serializer = serializer;
    }

    /**
     * Method writes the object as an array, for write uses classes that implement {@link IJsonWriter}
     * Write in sign "[...]", line of elements writes with comma
     *
     * @param object - object for writing to writer
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(Object object, IJsonWriter writer) {
        writer.writeArrayBegin();
        int length = getLength(object);
        for (int i = 0; i < length; i++) {
            serializer.serialize(get(object, i), writer);
            if (i != length - 1) writer.writeSeparator();
        }
        writer.writeArrayEnd();
    }
}
