package com.goitlearning.json.mapper;

import com.goitlearning.json.serializer.ISubElementSerializer;
import com.goitlearning.json.writer.IJsonWriter;

import java.util.Iterator;
import java.util.Map;

/**
 * Writes instances of {@link Map} to JSON, where key and value are separated by colon.
 */
public class MapMapper implements IJsonMapper<Map> {
    private ISubElementSerializer serializer;

    /**
     * Constructor contains parameter {@link #serializer}
     *
     * @param serializer - defines way to serialize child objects
     */
    public MapMapper(ISubElementSerializer serializer) {
        this.serializer = serializer;
    }

    /**
     * Method writes colon between key and value, for write uses classes that implement
     * {@link IJsonWriter}
     *
     * @param object - {@link Map} for writing to JSON.
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(Map object, IJsonWriter writer) {
        writer.writeObjectBegin();
        @SuppressWarnings("unchecked")
        Iterator<Map.Entry> iterator = object.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            serializer.serialize(entry.getKey(), writer);
            writer.writePropertySeparator();
            serializer.serialize(entry.getValue(), writer);
            if (iterator.hasNext()) writer.writeSeparator();
        }
        writer.writeObjectEnd();
    }
}
