package com.goitlearning.json.mapper;

import com.goitlearning.json.serializer.ISubElementSerializer;
import com.goitlearning.json.writer.IJsonWriter;

import java.util.Collection;
import java.util.Iterator;

/**
 * The class writes instances of {@link Collection} as an array.
 */
public class CollectionMapper implements IJsonMapper<Collection> {
    private ISubElementSerializer serializer;

    /**
     * Constructor contains parameter {@link #serializer}
     * @param serializer - defines way to serialize child objects
     */
    public CollectionMapper(ISubElementSerializer serializer) {
        this.serializer = serializer;
    }

    /**
     * Method writes instances of {@link Collection} as an array,
     * for write uses classes that implement {@link IJsonWriter}
     * Write in sign "[...]", line of elements writes with comma
     *
     * @param object - instance of {@link Collection} for writing to JSON.
     * @param writer - defines rules for writing to JSON.
     */
    @Override
    public void write(Collection object, IJsonWriter writer) {
        writer.writeArrayBegin();
        Iterator iterator = object.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            serializer.serialize(o, writer);
            if (iterator.hasNext()) writer.writeSeparator();
        }
        writer.writeArrayEnd();
    }
}
