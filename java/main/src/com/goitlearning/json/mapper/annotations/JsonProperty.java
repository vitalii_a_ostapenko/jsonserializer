package com.goitlearning.json.mapper.annotations;

import java.lang.annotation.*;

/**
 * An annotation that indicates this member should be serialized to JSON regardless of its modifiers.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonProperty {
}
