package com.goitlearning.json.mapper.annotations;

import java.lang.annotation.*;

/**
 * An annotation that indicates this member should be ignored in serialization to JSON regardless of its modifiers.
 * Members annotated with {@link JsonProperty} and {@link JsonSerializedName} also ignored.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonIgnore {
}
