package com.goitlearning.json.serializer;

import com.goitlearning.json.mapper.*;
import com.goitlearning.json.writer.IJsonWriter;
import com.goitlearning.json.writer.IndentedJsonWriter;
import com.goitlearning.json.writer.JsonWriter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is designed to convert objects into JSON, it is thread-safe.
 * {@link #setIndent(boolean)} must be synchronized manually.
 *
 * @see IJsonSerializer
 * {@link #defaultCharset} - used for defining default character encoding for {@link #serialize(Object, OutputStream)}.
 * {@link #indent} - used to define JSON with indents
 * {@link #mappers} - contain standard mappers for serializing different objects
 */
public class JsonSerializer implements IJsonSerializer {
    private Charset defaultCharset = Charset.forName("UTF-8");
    private boolean indent;
    private Map<Class<?>, IJsonMapper<?>> mappers;

    /**
     * Constructor adds to {@link #mappers} standard mappers.
     */
    public JsonSerializer() {
        mappers = new HashMap<>();
        mappers.put(Object[].class, new ArrayMapper(this::serialize));
        mappers.put(Collection.class, new CollectionMapper(this::serialize));
        mappers.put(Map.class, new MapMapper(this::serialize));
        mappers.put(Object.class, new CustomObjectMapper(this::serialize));
        mappers.put(String.class, new StringMapper());
        mappers.put(Boolean.class, new BooleanMapper());
        mappers.put(Number.class, new NumberMapper());
        mappers.put(null, new NullMapper());
        mappers.put(Character.class, new CharMapper());
    }

    // This thread-safe returns mapper from the mappers
    private IJsonMapper getMapper(@Nullable Object object) {
        if (object == null) {
            return mappers.get(null);
        }
        Class clazz = object.getClass();
        if (clazz.isArray()) {
            return mappers.get(Object[].class);
        } else if (mappers.containsKey(clazz)) {
            return mappers.get(clazz);
        } else if (object instanceof Number) {
            return mappers.get(Number.class);
        } else if (object instanceof Map) {
            return mappers.get(Map.class);
        } else if (object instanceof Collection) {
            return mappers.get(Collection.class);
        } else if (object instanceof Character) {
            return mappers.get(Character.class);
        } else {
            return mappers.get(Object.class);
        }
    }

    /**
     * Method called to check whether converted objects JSON will be with indents or no.
     *
     * @return current indent
     */
    @Override
    public boolean isIndent() {
        return indent;
    }

    /**
     * Method must be synchronized manually.
     *
     * @param value is used to determine JSON with indents when serializing
     */

    @Override
    public void setIndent(boolean value) {
        this.indent = value;
    }

    /**
     * Method serializes given object.
     *
     * @param object object for serialization
     * @return String of object that has been serialized
     * @throws IllegalArgumentException if object is null
     */
    @Override
    public String serialize(@NotNull Object object) {
        StringWriter writer = new StringWriter();
        serialize(object, writer);
        return writer.toString();
    }

    /**
     * Method serializes given object to stream.
     * It uses default charset {@link #defaultCharset}.
     *
     * @param object object for serialization
     * @param stream output stream for writing JSON into it
     * @throws IllegalArgumentException if object or stream is null
     */
    @Override
    public void serialize(@NotNull Object object, @NotNull OutputStream stream) {
        serialize(object, stream, defaultCharset);
    }

    /**
     * Method serializes object given object to stream using {@link Charset}.
     *
     * @param object  object for serialization
     * @param stream  output stream for writing JSON into it
     * @param charset used for defining character encoding
     * @throws IllegalArgumentException if object or stream or charset is null
     */
    @Override
    public void serialize(@NotNull Object object, @NotNull OutputStream stream, @NotNull Charset charset) {
        serialize(object, new OutputStreamWriter(stream, charset));
    }

    /**
     * Method serializes given object and create {@link IJsonWriter} accordingly to {@link #isIndent()}.
     *
     * @param object object for serialization
     * @param writer used for writing JSON into it
     * @throws IllegalArgumentException if object or writer is null
     */
    @Override
    public void serialize(@NotNull Object object, @NotNull Writer writer) {
        if (isIndent()) {
            serialize(object, new IndentedJsonWriter(writer));
        } else {
            serialize(object, new JsonWriter(writer));
        }
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*Mappers that contain child elements and there constructor require ISubElementSerializer
    * retrieve mapper and writes object*/
    @SuppressWarnings("unchecked")
    private void serialize(Object object, IJsonWriter writer) {
        IJsonMapper mapper = getMapper(object);
        mapper.write(object, writer);
    }
}
