package com.goitlearning.json.serializer;

import com.goitlearning.json.writer.IJsonWriter;

/**
 * Used for encapsulation serialization of child elements in {@link com.goitlearning.json.mapper.IJsonMapper}
 */
public interface ISubElementSerializer {
    /**
     * Writes given object to writer
     *
     * @param object - child element for serialization
     * @param writer - defines rules for writing to JSON
     */
    void serialize(Object object, IJsonWriter writer);
}
