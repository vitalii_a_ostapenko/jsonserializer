package com.goitlearning.json.serializer;

import java.io.OutputStream;
import java.io.Writer;
import java.nio.charset.Charset;

public interface IJsonSerializer {
    boolean isIndent();
    void setIndent(boolean value);
    String serialize(Object object);
    void serialize(Object object, OutputStream stream);
    void serialize(Object object, OutputStream stream, Charset charset);
    void serialize(Object object, Writer writer);
}
