package com.goitlearning.json.mapper;

import com.goitlearning.json.mapper.mocks.*;
import com.goitlearning.json.serializer.ISubElementSerializer;
import com.goitlearning.json.writer.IJsonWriter;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class CustomObjectMapperTest {
    private CustomObjectMapper mapper;
    private IJsonWriter writer;
    private ISubElementSerializer serializer;

    @Before
    public void setUp() throws Exception {
        writer = mock(IJsonWriter.class);
        serializer = mock(ISubElementSerializer.class);
        mapper = new CustomObjectMapper(serializer);
    }

    @Test
    public void writeIgnorePrivate() throws Exception {
        templateIgnoreFieldTest(new PrivateMock());
    }

    @Test
    public void writePublic() throws Exception {
        templateWriteFieldTest(new PublicMock());
    }

    @Test
    public void writeProtected() throws Exception {
        templateWriteFieldTest(new ProtectedMock());
    }

    @Test
    public void writePackageLocal() throws Exception {
        templateWriteFieldTest(new PackageLocalMock());
    }

    @Test
    public void writeIgnoreStatic() throws Exception {
        templateIgnoreFieldTest(new StaticMock());
    }

    @Test
    public void writeIgnoreTransient() throws Exception {
        templateIgnoreFieldTest(new TransientMock());
    }

    @Test
    public void writeIgnoreFieldWithJsonIgnore() throws Exception {
        templateIgnoreFieldTest(new JsonIgnoreFieldMock());
    }

    @Test
    public void writeFieldWithJsonProperty() throws Exception {
        templateWriteFieldTest(new JsonPropertyFieldMock());
    }

    @Test
    public void writeFieldWithJsonSerializedName() throws Exception {
        mapper.write(new JsonSerializedNameFieldMock(), writer);
        verify(writer).writeObjectBegin();
        verify(writer).writeString("code");
        verify(writer).writePropertySeparator();
        verify(serializer).serialize(0, writer);
        verify(writer).writeObjectEnd();
    }

    @Test
    public void writeFieldsIncludingSuper() throws Exception {
        mapper.write(new FieldsIncludingSuperMock(), writer);
        verify(writer).writeObjectBegin();
        verify(writer, times(2)).writeString("id");
        verify(writer, times(2)).writePropertySeparator();
        verify(writer).writeSeparator();
        verify(serializer, times(2)).serialize(0, writer);
        verify(writer).writeObjectEnd();
    }

    private <T> void templateIgnoreFieldTest(T t) {
        mapper.write(t, writer);
        verify(writer).writeObjectBegin();
        verify(writer, never()).writeString("id");
        verify(writer, never()).writePropertySeparator();
        verify(serializer, never()).serialize(0, writer);
        verify(writer).writeObjectEnd();
    }

    private <T> void templateWriteFieldTest(T t) {
        mapper.write(t, writer);
        verify(writer).writeObjectBegin();
        verify(writer).writeString("id");
        verify(writer).writePropertySeparator();
        verify(serializer).serialize(0, writer);
        verify(writer).writeObjectEnd();
    }
}