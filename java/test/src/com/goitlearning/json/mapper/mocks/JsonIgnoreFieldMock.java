package com.goitlearning.json.mapper.mocks;

import com.goitlearning.json.mapper.annotations.JsonIgnore;

public class JsonIgnoreFieldMock {
    @JsonIgnore
    public int id = 0;
}
