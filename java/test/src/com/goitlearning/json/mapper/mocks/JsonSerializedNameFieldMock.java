package com.goitlearning.json.mapper.mocks;

import com.goitlearning.json.mapper.annotations.JsonSerializedName;

public class JsonSerializedNameFieldMock {
    @JsonSerializedName("code")
    private int id = 0;
}
