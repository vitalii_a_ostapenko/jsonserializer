package com.goitlearning.json.writer;

import org.junit.Before;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;

import static org.junit.Assert.*;

public class IndentedJsonWriterTest {
    private Writer writer;
    private IJsonWriter jsonWriter;
    private String expected = "{\n  \"name\": 5,\n  [\n    1,\n    2\n  ]\n}";
    @Before
    public void setUp() throws Exception {
        writer = new StringWriter();
        jsonWriter = new IndentedJsonWriter(writer);
    }

    @Test
    public void writeIndent() throws Exception {
        jsonWriter.writeObjectBegin();
        jsonWriter.writeString("name");
        jsonWriter.writePropertySeparator();
        jsonWriter.writeNumber(5);
        jsonWriter.writeSeparator();
        jsonWriter.writeArrayBegin();
        jsonWriter.writeNumber(1);
        jsonWriter.writeSeparator();
        jsonWriter.writeNumber(2);
        jsonWriter.writeArrayEnd();
        jsonWriter.writeObjectEnd();
        assertEquals(expected, writer.toString());
    }
}