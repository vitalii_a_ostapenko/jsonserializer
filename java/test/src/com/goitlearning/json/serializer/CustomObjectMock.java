package com.goitlearning.json.serializer;

import java.util.*;

public class CustomObjectMock {
    char c = 'a';
    boolean isMock = true;
    List<String> list = new ArrayList<>();
    Map<String, String> map = new LinkedHashMap<>();

    public CustomObjectMock() {
        list.add("value1");
        list.add(null);

        map.put("key1", "value1");
        map.put("key2", "value2");
    }
}
