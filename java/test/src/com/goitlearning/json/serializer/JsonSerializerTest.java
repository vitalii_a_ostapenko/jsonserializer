package com.goitlearning.json.serializer;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JsonSerializerTest {
    private IJsonSerializer serializer = new JsonSerializer();
    private int[] nums = {1, 2, 3, 4};
    private String expected = "[1,2,3,4]";

    @Test
    public void serializeObject() throws Exception {
        assertEquals(expected, serializer.serialize(nums));
    }

    @Test
    public void serializeObjectOutputStream() throws Exception {
        String expected = "{\"c\":\"a\",\"isMock\":true,\"list\":[\"value1\",null]," +
                "\"map\":{\"key1\":\"value1\",\"key2\":\"value2\"}}";
        OutputStream outputStream = new ByteArrayOutputStream();
        serializer.serialize(new CustomObjectMock(), outputStream);
        String actual = outputStream.toString();
        assertEquals(expected, actual);
    }

    /*
    * The idea of this test is to run 100_000 times serialization on the same instance of JsonSerializer
    * asynchronously and add results to synchronizedSet, then check that set.size() == 1, it means that all
    * 100_000 times result was the same and then check with expected
    */
    @Test
    public void multithreadingSerialize() {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        Set<String> set = Collections.synchronizedSet(new HashSet<>());
        int count = 100_000;
        CountDownLatch countDownLatch = new CountDownLatch(count);
        for (int i = 0; i < count; i++) {
            executorService.execute(() -> {
                set.add(serializer.serialize(nums));
                countDownLatch.countDown();
            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        assertEquals(1, set.size());
        assertTrue(set.contains(expected));
    }
}